﻿using UnityEngine;

public class Emmit : IStrategy
{
    public void Perform(Transform transform, int performanceStrength)
    {
        transform.GetComponentInChildren<ParticleSystem>().Emit(performanceStrength);
    }
}
