﻿using UnityEngine;

public interface IStrategy
{
    void Perform(Transform transform, int performanceStrength);
}
