﻿using UnityEngine;

public class MoveForward : IStrategy
{
    public void Perform(Transform transform, int performanceStrength)
    {
        transform.position += transform.up * performanceStrength *Time.deltaTime;
    }
}
