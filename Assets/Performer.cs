﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Performer : Singleton<Performer>
{
    public int performanceStrength;

    private IStrategy _strategy;

    private void Update()
    {
        if (_strategy != null) 
            _strategy.Perform(transform, performanceStrength);
    }

    public void SetStrategy(IStrategy strategy)
    {
        _strategy = strategy;
    }
}
