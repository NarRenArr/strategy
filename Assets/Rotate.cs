﻿using UnityEngine;

public class Rotate : IStrategy
{
    public void Perform(Transform transform, int performanceStrength)
    {
        transform.Rotate(0, 0, 90 * performanceStrength * Time.deltaTime);
    }
}
