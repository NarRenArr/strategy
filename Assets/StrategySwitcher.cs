﻿using UnityEngine;

public class StrategySwitcher : MonoBehaviour
{
    public void SetMoveForward()
    {
        MoveForward strategy = new MoveForward();
        Performer.Instance.SetStrategy(strategy);
    }

    public void SetRotate()
    {
        Rotate strategy = new Rotate();
        Performer.Instance.SetStrategy(strategy);
    }

    public void SetEmmit()
    {
        Emmit strategy = new Emmit();
        Performer.Instance.SetStrategy(strategy);
    }
}
